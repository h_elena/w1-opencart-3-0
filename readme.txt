1. Register on the site https://www.walletone.com/ru/merchant/
2. Download the module files on the repository https://bitbucket.org/h_elena/w1-opencart/downloads and install.
3. Activate the module.
4. Instructions for configuring the module is in site https://www.walletone.com/ru/merchant/modules/opencart-cms/ .
